import {defineConfig} from "vite"
import react from "@vitejs/plugin-react"
import svgr from "vite-plugin-svgr"
import mdx from "@mdx-js/rollup"
import remarkFrontmatter from "remark-frontmatter"
import pages from "@__mazerty__/rollup-plugin-pages"
import tailwind from "tailwindcss"
import typography from "@tailwindcss/typography"
import defaultTheme from "tailwindcss/defaultTheme.js"
import colors from "tailwindcss/colors.js"

export default defineConfig({
    plugins: [
        react(),
        svgr(),
        mdx({remarkPlugins: [remarkFrontmatter]}),
        pages(__dirname + "/src/blog/pages")
    ],
    css: {
        postcss: {
            plugins: [
                tailwind({
                    content: ["./index.html", "./src/**/*.jsx"],
                    theme: {
                        extend: {
                            fontFamily: {
                                sans: ["Ubuntu", ...defaultTheme.fontFamily.sans],
                                specialelite: ["Special Elite", ...defaultTheme.fontFamily.sans]
                            },
                            boxShadow: {
                                picture: "0 4px 8px 4px rgba(0, 0, 0, 0.3)",
                                diffuse: "0 4px 12px 0 rgba(0, 0, 0, 0.3)"
                            },
                            colors: {
                                gray: {
                                    ...colors.slate,
                                    697: "#364458",
                                    703: "#303e52"
                                }
                            },
                            minHeight: {
                                80: "80vh"
                            },
                            columns: {
                                "4xs": "9rem"
                            }
                        }
                    },
                    plugins: [typography()],
                    future: {
                        hoverOnlyWhenSupported: true
                    }
                })
            ]
        }
    }
})
