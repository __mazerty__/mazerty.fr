import ReactDOM from "react-dom/client"
import React from "react"
import {createBrowserRouter, redirect, RouterProvider} from "react-router"
import Layout from "./Layout"
import pages from "virtual:pages"
import "./index.css"

const Root = React.lazy(() => import("./root/Root"))
const Home = React.lazy(() => import("./home/Home"))
const Blog = React.lazy(() => import("./blog/Blog"))
const Wrap = React.lazy(() => import("./blog/Wrap"))
const Lynx = React.lazy(() => import("./lynx/Lynx"))

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={createBrowserRouter([{
            element: <Layout/>,
            children: [{
                index: true,
                element: <Root/>
            }, {
                path: "home",
                element: <Home/>
            }, {
                path: "blog",
                children: [{
                    index: true,
                    element: <Blog/>
                }, {
                    element: <Wrap/>,
                    children: pages.map(page => {
                        const Page = React.lazy(page.import)
                        return {
                            path: page.key,
                            element: <Page/>,
                            handle: page
                        }
                    })
                }, {
                    path: "*",
                    loader: () => redirect("/blog")
                }]
            }, {
                path: "lynx",
                element: <Lynx/>
            }, {
                path: "*",
                loader: () => redirect("/")
            }]
        }])}/>
    </React.StrictMode>
)
