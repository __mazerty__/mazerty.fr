import bookmarks from "./bookmarks.js"
import {useState} from "react"
import {Set} from "immutable"

const availableLabels = new Set(bookmarks.flatMap(bookmark => bookmark.labels))

export default () => {
    const [selectedLabels, setSelectedLabels] = useState(new Set())

    const filteredBookmarks = bookmarks.filter(bookmark => selectedLabels.isSubset(bookmark.labels))
    const filteredLabels = new Set(filteredBookmarks.flatMap(bookmark => bookmark.labels))

    return <div className="border border-gray-600 rounded shadow-diffuse">
        <div className="bg-gray-697">
            {availableLabels.sort().map(label => {
                const selected = selectedLabels.has(label)
                const excluded = !selectedLabels.isEmpty() && !filteredLabels.has(label)

                return <button key={label}
                               onClick={() => setSelectedLabels(selected ? selectedLabels.delete(label) : (excluded ? Set.of(label) : selectedLabels.add(label)))}
                               className={"px-2.5 py-1.5 hover:bg-gray-500" + (selected ? " bg-gray-600" : (excluded ? " opacity-30" : ""))}>{label}</button>
            })}
        </div>
        <div className="px-4 py-3 text-gray-400 bg-gray-703">
            {filteredBookmarks.map(bookmark => (
                <div key={bookmark.url} className="truncate">
                    {bookmark.title && <a href={bookmark.url} className="mr-3 text-gray-200 hover:underline">{bookmark.title}</a>}
                    <a href={bookmark.url} className="hover:underline">{bookmark.url}</a>
                </div>
            ))}
        </div>
    </div>
}
