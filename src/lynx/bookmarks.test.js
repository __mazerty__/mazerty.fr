import {describe, expect, test} from "vitest"
import bookmarks from "./bookmarks.js"
import {Set} from "immutable"

describe("a bookmark", () => {
    test("must have an url property that is a string", () => {
        bookmarks.forEach(bookmark => {
            expect(bookmark).toHaveProperty("url")
            expect(bookmark.url).toEqual(expect.any(String))
        })
    })
    test("must have a labels property that is an array of strings", () => {
        bookmarks.forEach(bookmark => {
            expect(bookmark).toHaveProperty("labels")
            expect(bookmark.labels).toEqual(expect.any(Array))
            bookmark.labels.forEach(label => expect(label).toEqual(expect.any(String)))
        })
    })
    test("may have a title property, but if it does, it must be a string", () => {
        bookmarks.forEach(bookmark => {
            bookmark.title && expect(bookmark.title).toEqual(expect.any(String))
        })
    })
    test("can only have three properties : title, url and labels", () => {
        bookmarks.forEach(bookmark => {
            expect(["title", "url", "labels"]).toEqual(expect.arrayContaining(Object.keys(bookmark)))
        })
    })
})

describe("bookmarks", () => {
    test("must be unique", () => {
        expect(new Set(bookmarks.map(bookmark => bookmark.url)).size).toEqual(bookmarks.size)
    })
})
