import {Outlet, useMatches} from "react-router"

export default () => {
    const current_page = useMatches().find(i => i.handle)?.handle

    return <>
        <div className="text-gray-400">{current_page.date}</div>
        <div className="prose prose-invert max-w-none overflow-hidden">
            <Outlet/>
        </div>
    </>
}
