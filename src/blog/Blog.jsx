import {Link} from "react-router"
import pages from "virtual:pages"

export default () => pages
    .sort((a, b) => -a.date.localeCompare(b.date))
    .map(page => (
        <Link to={page.key} key={page.key}>
            <div className="px-5 py-4 mb-4 border border-gray-600 rounded shadow-diffuse">
                <div className="flex flex-col-reverse sm:flex-row">
                    <div className="text-xl mr-auto">{page.title}</div>
                    <div className="text-sm text-gray-400">{page.date}</div>
                </div>
                <div className="text-gray-400">{page.description}</div>
            </div>
        </Link>
    ))
