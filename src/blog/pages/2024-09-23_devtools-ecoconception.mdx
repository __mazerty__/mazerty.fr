---
title: DevTools & Écoconception
description: Ou comment les DevTools peuvent aider à analyser l'écoconception d'un site web
---
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faStopwatch} from "@fortawesome/free-solid-svg-icons"

import inspector from "./2024-09-23_devtools-ecoconception/inspector.webp"
import console from "./2024-09-23_devtools-ecoconception/console.webp"
import debuggerz from "./2024-09-23_devtools-ecoconception/debugger.webp"
import network1 from "./2024-09-23_devtools-ecoconception/network1.webp"
import network2 from "./2024-09-23_devtools-ecoconception/network2.webp"
import network3 from "./2024-09-23_devtools-ecoconception/network3.webp"
import network4 from "./2024-09-23_devtools-ecoconception/network4.webp"
import network5 from "./2024-09-23_devtools-ecoconception/network5.webp"
import network6 from "./2024-09-23_devtools-ecoconception/network6.webp"

# DevTools & Écoconception

Les `DevTools` sont ces outils présents dans la plupart des navigateurs qui permettent de concevoir et debugger des sites web.
Mais ces outils ne sont pas réservés qu'au développement et, même s'ils restent techniques, avec un peu d'entraînement il est possible de les utiliser lors d'analyses d'écoconception.
Ils permettent d'aller au-delà des métriques type EcoIndex, en "espionnant" ce qui se passe côté navigateur/client, mais aussi côté serveur.

Pour des raisons pratiques, la suite de cet article part du principe que vous utilisez Firefox en anglais.
Les DevTools de Chrome, Brave, Opera ou Edge fonctionnent globalement de la même manière.

Si vous préférez une version vidéo, voici le [replay](https://peertube.designersethiques.org/w/67vXbDpAPptd6qg8cwE1SK) de la Communauté Écoconception des Designers Éthiques du 26 septembre 2024.


### Disclaimer

Cet article est une tentative de vulgarisation technique à destination notamment des designers, et en tant que tel des erreurs ou des biais cognitifs ont pu s'y glisser.
Si c'est le cas, n'hésitez pas à me contacter et je prendrai le temps de corriger :)
Vous pouvez aussi contribuer directement à cet article sur [GitLab](https://gitlab.com/__mazerty__/mazerty.fr/-/blob/omega/src/blog/pages/2024-09-23_devtools-ecoconception.mdx) si vous le souhaitez !


## Let's go!

Il y a plusieurs façons d'accéder aux DevTools :
* En faisant un clic droit sur n'importe quel élément d'une page web, puis en sélectionnant `Inspect`,
* En ouvrant le menu principal, puis en sélectionnant `More Tools` puis `Web Developer Tools`,
* En utilisant le raccourci clavier `Ctrl + Maj + I` ou `Cmd + Opt + I`.


## Inspector

Le premier outil qui s'affiche à l'ouverture des DevTools est l'`Inspector` :

<img loading="lazy" src={inspector} alt="Une copie d'écran de l'outil Inspector, montrant une importante arborescence HTML"/>

À la différence de l'action `View Page Source` qui affiche le code HTML qui a été _téléchargé_ par le navigateur,
l'Inspector permet de parcourir la structure du site web _tel qu'il apparait_, c'est-à-dire une fois que le code Javascript a été exécuté.

Cet outil contient beaucoup trop d'informations pour notre besoin ici, mais il peut quand même donner un aperçu de la qualité globale de la page :
* Si l'arborescence à gauche montre par exemple une utilisation exagérée de `div` encapsulées, c'est probablement le signe d'un développement hâtif ou de l'utilisation d'un framework.
* Les noms des classes CSS peuvent permettre de déduire l'utilisation d'un framework CSS particulier.
Par exemple, `<div class="w-full max-w-4xl mx-auto">` pour Tailwind, `<div class="col-xl-3 col-md-6 mb-4">` pour Bootstrap.
* À l'opposé, un site visuellement riche mais dont les éléments ne contiennent peu ou pas de classes CSS peut être révélateur d'une bonne maîtrise (et donc optimisation) du CSS.

Après, est-ce que c'est mal d'utiliser un framework CSS ?
[Ça dépend.](https://joshcollinsworth.com/blog/tailwind-is-smart-steering)
C'est un éternel débat, mon avis étant qu'il vaut mieux un framework CSS à jour et optimisé plutôt qu'un fichier CSS rempli de copiés-collés de StackOverflow ;)


## Console

<img loading="lazy" src={console} alt="Une copie d'écran de l'outil Console, montrant une erreur et plusieurs warnings"/>

L'outil `Console` permet d'afficher les messages liés à l'interprétation par le navigateur du code HTML, CSS et Javascript du site.
Là aussi, on peut se retrouver noyé sous la quantité d'information, mais le nombre d'`Errors` (en rouge) et de `Warnings` (en jaune) peut être une métrique intéressante.

Une erreur peut signifier un bug plus ou moins critique. Un warning, du vieux code sans réel impact mais qui n'a pas été nettoyé.
Dans tous les cas, un nombre important de tels messages est le signe d'une maintenance un peu trop légère, avec pour conséquence le téléchargement de code potentiellement inutile.

Vous l'avez peut-être remarqué dans la copie d'écran, mais les DevTools peuvent révéler beaucoup d'informations sur un site, parfois à l'insu de leur propriétaire.
Ici par exemple, on apprend dans le deuxième warning que ce site est généré par Drupal en version 9.4.0.
Au-delà de la faille de sécurité que cela représente, ce genre d'information permet d'aller plus loin dans l'analyse : Drupal est utilisé côté serveur, quel est l'impact en terme d'écoconception ?


## Debugger

<img loading="lazy" src={debuggerz} alt="Une copie d'écran de l'outil Debugger, montrant l'arborescence de fichiers et le contenu d'un fichier Javascript"/>

L'outil `Debugger` est normalement utilisé lors du développement pour parcourir le code Javascript et trouver la source d'un problème.
Dans notre cas, il nous montre la structure des fichiers Javascript qui ont été téléchargés jusqu'à présent, et va permettre de glâner quelques informations supplémentaires :

* Si un nom de fichier contient des caractères aléatoires comme `5Pp8uZLNtMhO5`, c'est sûrement qu'un outil de _packaging_ comme Webpack ou Vite a été utilisé.
Ces outils génèrent le code Javascript au moment du déploiement d'une nouvelle version du site, afin qu'il soit accessible de manière _statique_ aux navigateurs.
C'est souvent la meilleure solution car cela permet d'optimiser le code en amont d'une part, et d'autre part le téléchargement ne demande quasiment aucun traitement côté serveur.
C'est aussi idéal pour bénéficier des différents types de _cache_ qui existent.

* Si un nom de fichier contient un point d'interrogation comme `?v=9.5.11`, c'est que ce fichier a été généré de manière _dynamique_.
C'est-à-dire que le serveur a dû analyser les paramètres de la requête (ce qu'il y a après le point d'interrogation) pour déterminer quoi répondre.
Ce n'est pas forcément un problème, car si les paramètres sont toujours les mêmes, certains mécanismes de cache s'activeront.
Mais s'ils changent régulièrement, comme potentiellement ici `_Incapsula_Resource?SWJIYLWA=719d34d31c8e3a6e6fffd425f7e032f3&ns=2&cb=905485171`, le cache sera ignoré et le serveur devra regénérer un nouveau fichier à chaque fois.

* Si un nom de fichier contient `.min.js` ou si son contenu a l'air très condensé sur quelques lignes (par exemple `function A0(e,t){for(var n=0;n<t.length;n++)...`), c'est qu'il est _minifié_.
La minification est une technique d'optimisation qui permet de réduire la taille d'un fichier Javascript et donc son temps de chargement.
C'est une bonne pratique, donc si vous trouvez un fichier Javascript bien lisible et formaté, vous pouvez suggérer de le minifier :)

* Vous voyez le message `No source map found` en bas de la copie d'écran ?
Un _source map_ est un type de fichier très spécifique qui peut être utilisé lors du développement, mais qui prend inutilement de la place une fois le site déployé.
Si en ouvrant un fichier ce message change ou disparait, c'est que des source maps ont été déployés par erreur, vous pouvez également le remonter.


## Network

<img loading="lazy" src={network1} alt="Une copie d'écran de l'outil Network, juste après son ouverture"/>

L'outil `Network` permet d'analyser les requêtes émises par le navigateur, c'est-à-dire les différents transferts de données entre le navigateur et le serveur.
Tout le code Javascript, HTML et CSS nécessaire à l'affichage d'un site web est le résultat de telles requêtes.
Commençons par faire une rapide analyse de performance en cliquant sur le bouton <FontAwesomeIcon icon={faStopwatch}/> :

<img loading="lazy" src={network2} alt="Une copie d'écran de l'outil Network, avec les résultats de l'analyse de performance"/>

Ces graphiques résument pas mal d'informations utiles.
Une fois n'est pas coutume, commençons par celui de droite.
Il permet de comparer, par type de contenu (HTML, CSS, Javascript, images, etc.), la taille _réelle_ (`Size`) et _transférée_ (`Transferred`) des requêtes, ainsi que leur temps d'exécution :
* Une page bien optimisée aura relativement peu de Javascript par rapport au HTML et au CSS par exemple.
* Maintenant, pourquoi y aurait-il une différence entre taille réelle et transférée ?
Il se trouve qu'il est possible de configurer un serveur pour que les requêtes soient automatiquement compressées afin de diminuer le temps de téléchargement.
C'est fortement recommandé, même si l'efficacité de cette compression peut varier en fonction du type de contenu ou du moment où elle est réalisée (en amont lors du déploiement, ou à chaque requête).
Si par contre vous ne constatez aucune différence entre ces valeurs, vous pouvez sans crainte recommander d'activer cette compression.

Le graphique de gauche est semblable, à ceci près qu'il montre les performances de la page quand elle est chargée _une seconde fois_, et que le cache est utilisé :
* `Size` représente alors la taille des élements du cache réutilisés automatiquement par le navigateur, et `Transferred` ce qui était manquant et a été téléchargé à nouveau.
* Pourquoi est-ce si important ?
Parce que si la page est amenée à être appelée très souvent, il ne faudrait pas que tous les élements de la page soient téléchargés à chaque fois, même s'ils n'ont pas changé.
* Dans la copie d'écran on peut voir qu'il y a une fraction du Javascript et probablement une image qui sont téléchargés.
Est-ce que c'est voulu, parce que le Javascript représente des données susceptibles d'avoir changé, ou que l'image est différente ?
Cela peut valoir le coup de se pencher dessus car il y a peut-être une optimisation à faire de ce côté-là.

Il y a une dernière chose dont nous n'avons pas parlé, ce sont les temps de chargement.
Pour tout ce qui est fichier statique, comme une image, le temps de chargement est en général proportionnel à la qualité de la connexion internet.
Mais si une requête nécessite l'accès à une base de données par exemple, le serveur passera un certain temps à traiter cette requête avant même de commencer à transmettre la réponse.
Pour estimer combien de requêtes sont dans ce cas, il faut cliquer sur `Back` :

<img loading="lazy" src={network3} alt="Une copie d'écran de l'outil Network, avec les détails des requêtes"/>

On a ici le détail des requêtes qui ont permis de générer les graphiques précédents.
Ce qui nous intéresse c'est la colonne la plus à droite, qui contient une sorte de frise chronologique représentant l'ordre dans lequel les requêtes sont exécutées et le temps qu'elles ont mis.
Pour que ça soit un peu plus parlant, cochez la case `Disable Cache` en haut à droite pour désactiver le système de cache et forcer le navigateur à tout télécharger.
Cliquez également sur `No Throttling` et sélectionnez `Regular 4G/LTE` pour simuler une connexion 4G.
Enfin rafraîchissez la page :

<img loading="lazy" src={network4} alt="Une copie d'écran de l'outil Network, une fois la page rechargée"/>

Pour chaque requête on peut distinguer deux couleurs : le bleu est le temps passé à attendre que le serveur commence à répondre, le vert est le temps passé à télécharger la réponse.
Que remarque-t-on ici ?
En réalité pas grand-chose : les temps d'attente sont relativement courts, et le temps de téléchargement est représentatif d'une connexion 4G.
Que se passe-t-il si on fait une recherche par exemple ?

<img loading="lazy" src={network5} alt="Une copie d'écran de l'outil Network, montrant les requêtes d'une page de recherche"/>

Là c'est tout de suite plus intéressant !
Regardez la première requête : il s'agit de code HTML comme l'indique la colonne `Type`, et elle a mis presque deux secondes à répondre.
Que peut-on en déduire ?
Eh bien que ce code HTML n'est pas statique comme ça peut être le cas dans d'autres sites, ou sur d'autres pages de ce même site, et que le serveur a pris un temps non négligeable pour générer les résultats de cette recherche.
À quoi ressemble ce code HTML ?
Est-ce que c'est aussi Drupal qui gère le moteur de recherche ?
Maintenant c'est à vous de poursuivre l'investigation ;)

Attention tout de même car certains mécanismes côté serveur permettent d'optimiser les traitements lorsqu'on lui demande plusieurs fois la même chose.
Voici ce qui se passe si on rafraîchit la page sans rien changer :

<img loading="lazy" src={network6} alt="Une copie d'écran de l'outil Network, montrant que la recherche est maintenant plus rapide"/>

Cette fois les résultats s'affichent beaucoup plus rapidement, et pour rappel cela n'a rien à voir avec le cache du navigateur.
Pour que vos tests soient plus représentatifs, n'hésitez pas à varier les requêtes envoyées au serveur.

Enfin un dernier point : vous l'avez peut-être remarqué mais le navigateur peut exécuter plusieurs requêtes en parallèle.
Ne vous fiez donc pas simplement au temps que met la page à s'afficher, il y a peut-être plusieurs requêtes qui s'exécutent en même temps, sur des microservices différents, qui sait ?


## Conclusion

Cet article n'est évidemment qu'un aperçu des fonctionnalités des DevTools, et j'ai volontairement simplifié certains détails pour ne perdre personne.
Je n'ai qu'une vague idée de ce que représente une analyse complète d'écoconception, mais j'espère que cet article vous aura donné quelques éléments techniques pour la rendre plus pertinente :)

## Liens

* [Firefox DevTools User Docs](https://firefox-source-docs.mozilla.org/devtools-user)
* [Chrome DevTools](https://developer.chrome.com/docs/devtools/overview)
* [La page "Outils" de l'Institut du Numérique Responsable](https://institutnr.org/outils-ecoconception-accessibilite)
* [HTTP Caching explained](https://http.dev/caching)
