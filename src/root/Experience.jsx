import {Transition} from "@headlessui/react"
import {ChevronDoubleDownIcon, ChevronRightIcon} from "@heroicons/react/24/outline"
import experience from "./experience.js"
import {useState} from "react"
import {Set} from "immutable"

const initialOpenItems = new Set(experience.filter(activity => activity.title && activity.open).map(activity => activity.dates))
const openableItems = new Set(experience.filter(activity => activity.title).map(activity => activity.dates))

export default () => {
    const [openItems, setOpenItems] = useState(initialOpenItems)

    const areAllItemsOpen = openItems.equals(openableItems)

    return <>
        <div className="pb-3 pt-10 flex items-center">
            <div className="mr-auto font-specialelite text-2xl">Experience</div>
            <div className="pl-3 py-1 text-gray-400 cursor-pointer print:hidden">
                <ChevronDoubleDownIcon
                    className={"h-6 w-6 transition-transform transform " + (areAllItemsOpen ? "rotate-180" : "rotate-0")}
                    onClick={() => setOpenItems(areAllItemsOpen ? new Set() : openableItems)}/>
            </div>
        </div>
        <div className="border border-gray-600 rounded divide-y divide-gray-600 shadow-diffuse">
            {experience.map(activity => {
                const isOpen = openItems.has(activity.dates)

                if (activity.title) {
                    return <div key={activity.dates}>
                        <div className="px-4 py-3 flex items-center bg-gray-697 cursor-pointer print:bg-transparent" onClick={() => setOpenItems(isOpen ? openItems.delete(activity.dates) : openItems.add(activity.dates))}>
                            <ChevronRightIcon className={"h-4 w-4 mr-2 text-gray-400 transition-transform transform " + (isOpen ? "rotate-90" : "rotate-0")}/>
                            <div className="w-full flex flex-col sm:flex-row">
                                <div className="mr-auto">{activity.title}</div>
                                {!isOpen && activity.caption && <div className="hidden md:block mr-10">{activity.caption}</div>}
                                <div className="w-36 text-gray-400">{activity.dates}</div>
                            </div>
                        </div>
                        <Transition
                            show={isOpen}
                            className="overflow-hidden"
                            enter="transition-[max-height] ease-in"
                            enterFrom="max-h-0"
                            enterTo="max-h-96"
                            leave="transition-[max-height] ease-out"
                            leaveFrom="max-h-96"
                            leaveTo="max-h-0">
                            <div className="px-5 py-4 bg-gray-703 print:bg-transparent">
                                {activity.company && <div><span className="text-gray-400">Company:</span> {activity.company}</div>}
                                {activity.project && <div><span className="text-gray-400">Project:</span> {activity.project}</div>}
                                {activity.role && <div><span className="text-gray-400">Role:</span> {activity.role}</div>}
                                {activity.employer && <div className="mt-2"><span className="text-gray-400">Consulting for:</span> {activity.employer}</div>}
                            </div>
                        </Transition>
                    </div>
                } else {
                    return <div key={activity.dates} className="px-4 py-3 flex flex-col sm:flex-row text-gray-500">
                        <div className="ml-6 mr-auto">Sabbatical</div>
                        <div className="w-36 ml-6">{activity.dates}</div>
                    </div>
                }
            })}
        </div>
    </>
}
