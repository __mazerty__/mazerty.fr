export default [{
    name: "Java",
    skills: [
        {name: "Java", score: 90},
        {name: "Kotlin", score: 60, like: true},
        {name: "Maven", score: 75},
        {name: "Gradle", score: 30},
        {name: "Spring", score: 60},
        {name: "jOOQ", score: 60, like: true},
        {name: "MyBatis", score: 75},
        {name: "Flyway", score: 60},
        {name: "JUnit", score: 60},
        {name: "DbUnit", score: 75}
    ],
    todos: [
        {name: "GraalVM"},
        {name: "Vert.x"}
    ]
}, {
    name: "Python",
    skills: [
        {name: "Python 3", score: 75, like: true},
        {name: "venv", score: 60},
        {name: "argparse", score: 90},
        {name: "Jinja2", score: 75, like: true},
        {name: "pytest", score: 60, like: true},
        {name: "Flask", score: 30}
    ],
    todos: [
        {name: "Poetry"},
        {name: "Click"},
        {name: "SQLAlchemy"},
        {name: "Twisted"},
        {name: "Tornado"}
    ]
}, {
    name: "Web",
    skills: [
        {name: "HTML 5", score: 75},
        {name: "CSS 3", score: 60},
        {name: "JavaScript", score: 90},
        {name: "TypeScript", score: 60},
        {name: "Vite", score: 60, like: true},
        {name: "React", score: 75, like: true},
        {name: "Tailwind", score: 75, like: true},
        {name: "Vitest", score: 45, like: true},
        {name: "Playwright", score: 45}
    ],
    todos: [
        {name: "Redux"},
        {name: "Lit"},
        {name: "Animations"}
    ]
}, {
    name: "Data",
    skills: [
        {name: "Oracle", score: 75},
        {name: "PostgreSQL", score: 60, like: true},
        {name: "SQL", score: 90},
        {name: "EventBridge", score: 60, like: true},
        {name: "DynamoDB", score: 45}
    ],
    todos: [
        {name: "Kafka", like: true},
        {name: "RabbitMQ"},
        {name: "etcd"},
        {name: "EventStoreDB", like: true}
    ]
}, {
    name: "System",
    skills: [
        {name: "Ubuntu", score: 75, like: true},
        {name: "RHEL", score: 60},
        {name: "Ansible", score: 60},
        {name: "cloud-init", score: 60, like: true},
        {name: "Docker", score: 60},
        {name: "LXD", score: 75, like: true},
        {name: "VirtualBox", score: 75},
        {name: "KVM/QEMU", score: 45, like: true},
        {name: "Nginx", score: 60, like: true},
        {name: "nftables", score: 45}
    ],
    todos: [
        {name: "Incus", like: true},
        {name: "containerd"},
        {name: "Podman"},
        {name: "Kubernetes"},
        {name: "NixOS"}
    ]
}, {
    name: "Cloud",
    skills: [
        {name: "AWS", score: 75},
        {name: "OpenStack", score: 45, like: true}
    ],
    todos: [
        {name: "GCP"},
        {name: "Infomaniak"}
    ]
}, {
    name: "Tools",
    skills: [
        {name: "Git", score: 90, like: true},
        {name: "JetBrains", score: 75, like: true},
        {name: "GitLab CI", score: 60, like: true},
        {name: "GitHub Actions", score: 45},
        {name: "Jenkins", score: 60},
        {name: "Terraform", score: 75}
    ],
    todos: [
        {name: "Prometheus", like: true},
        {name: "SigNoz"},
        {name: "Graylog"},
        {name: "OpenObserve"},
        {name: "Gatling"},
        {name: "Concourse"}
    ]
}, {
    name: "Practices",
    skills: [
        {name: "Agile", score: 90, like: true},
        {name: "DevOps", score: 75, like: true},
        {name: "Accelerate", score: 45},
        {name: "Testing", score: 90},
        {name: "Event sourcing", score: 45, like: true},
        {name: "Training", score: 60},
        {name: "Management", score: 45},
        {name: "Open source", like: true},
        {name: "K.I.S.S.", like: true}
    ],
    todos: [
        {name: "DDD"}
    ]
}, {
    name: "Languages",
    skills: [
        {name: "French", score: 90},
        {name: "English", score: 75, like: true},
        {name: "German", score: 45}
    ],
    todos: [
        {name: "Japanese", like: true}
    ]
}]
