import Intro from "./Intro"
import Skills from "./Skills"
import Experience from "./Experience"
import Training from "./Training"

export default () => <>
    <Intro/>
    <Skills/>
    <Experience/>
    <Training/>
</>
