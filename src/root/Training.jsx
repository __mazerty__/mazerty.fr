import {Transition} from "@headlessui/react"
import {ChevronRightIcon} from "@heroicons/react/24/outline"
import training from "./training.js"
import {useState} from "react"
import {Set} from "immutable"
import {ChevronDoubleDownIcon} from "@heroicons/react/24/outline/index.js"

const openableItems = new Set(training.map(activity => activity.date))

export default () => {
    const [openItems, setOpenItems] = useState(new Set())

    const areAllItemsOpen = openItems.equals(openableItems)

    return <>
        <div className="pb-3 pt-10 flex items-center">
            <div className="mr-auto font-specialelite text-2xl">Training <span className="text-gray-400">+ education</span></div>
            <div className="pl-3 py-1 text-gray-400 cursor-pointer print:hidden">
                <ChevronDoubleDownIcon
                    className={"h-6 w-6 transition-transform transform " + (areAllItemsOpen ? "rotate-180" : "rotate-0")}
                    onClick={() => setOpenItems(areAllItemsOpen ? new Set() : openableItems)}/>
            </div>
        </div>
        <div className="border border-gray-600 rounded divide-y divide-gray-600 shadow-diffuse">
            {training.map(activity => {
                const isOpen = openItems.has(activity.date)

                return <div key={activity.date}>
                    <div className="px-4 py-3 flex items-center bg-gray-697 cursor-pointer print:bg-transparent" onClick={() => setOpenItems(isOpen ? openItems.delete(activity.date) : openItems.add(activity.date))}>
                        <ChevronRightIcon className={"h-4 w-4 mr-2 text-gray-400 transition-transform transform " + (isOpen ? "rotate-90" : "rotate-0")}/>
                        <div className="w-full flex flex-col sm:flex-row">
                            <div className="mr-auto">{activity.title}</div>
                            <div className="w-12 text-gray-400">{activity.date}</div>
                        </div>
                    </div>
                    <Transition
                        show={isOpen}
                        className="overflow-hidden"
                        enter="transition-[max-height] ease-in"
                        enterFrom="max-h-0"
                        enterTo="max-h-96"
                        leave="transition-[max-height] ease-out"
                        leaveFrom="max-h-96"
                        leaveTo="max-h-0">
                        <div className="px-5 py-4 bg-gray-703 print:bg-transparent">
                            <div>{activity.description}</div>
                            {activity.location && <div>{activity.location}</div>}
                        </div>
                    </Transition>
                </div>
            })}
        </div>
    </>
}
