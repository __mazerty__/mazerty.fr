export default [{
    title: "RHCSA Rapid Track Course",
    date: "2019",
    description: "Red Hat System Administration I and II consolidated over 4 days"
}, {
    title: "DUT Informatique",
    date: "2005",
    description: "Option Génie Informatique",
    location: "IUT Lyon I @ Villeurbanne, France"
}, {
    title: "Baccalauréat Scientifique",
    date: "2001",
    description: "Option SVT, Spécialité Physique-Chimie, Mention Bien",
    location: "Lycée Montchapet @ Dijon, France"
}]
