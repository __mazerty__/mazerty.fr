export default [{
    title: "Technical Lead",
    open: true,
    caption: "Pixel @ Bedrock Streaming",
    dates: "2023/05 - Now",
    company: "Bedrock Streaming",
    project: "Pixel, a serverless video transcoding & packaging workflow hosted on AWS",
    employer: "Zenika"
}, {
    title: "Technical Lead & Engineering Manager",
    open: true,
    caption: "SmartConnect @ Enedis",
    dates: "2022/03 - 2023/04",
    company: "Enedis",
    project: "SmartConnect, an IOT-based electrical grid monitoring platform hosted on AWS",
    employer: "Zenika"
}, {
    dates: "2020/09 - 2022/02"
}, {
    title: "DevOps Engineer",
    caption: "ZSEv2 @ Enedis",
    dates: "2019/09 - 2020/08",
    company: "Enedis",
    project: "ZSEv2, an OpenStack private cloud with full CI/CD suite",
    role: "Ansible & Test specialist",
    employer: "Groupe Open"
}, {
    title: "Junior Cloud Architect",
    caption: "Episeq CS @ bioMérieux",
    dates: "2019/12",
    company: "bioMérieux",
    project: "Episeq CS, a protein analysis tool for pathogen monitoring",
    role: "AWS architecture audit",
    employer: "Groupe Open"
}, {
    title: "Senior Developer",
    caption: "LinkyDep @ Enedis",
    dates: "2018/12 - 2019/08",
    company: "Enedis",
    project: "LinkyDep, nationwide deployment of the French Smart Meter",
    role: "Java development & mentoring program for fresh graduate developers",
    employer: "Groupe Open"
}, {
    title: "Junior Cloud Architect",
    caption: "New TechLib @ bioMérieux",
    dates: "2018/11",
    company: "bioMérieux",
    project: "New TechLib, an overhaul of their Documentation Management System",
    role: "AWS architecture review & developer support",
    employer: "Groupe Open"
}, {
    dates: "2018/06 - 2018/11"
}, {
    title: "Senior Developer",
    caption: "MobileFactory @ Enedis",
    dates: "2017/10 - 2018/05",
    company: "Enedis",
    project: "MobileFactory, a small Rapid Application Development team",
    role: "Java/Angular development & deployment",
    employer: "Groupe Open"
}, {
    title: "Junior DevOps Engineer",
    caption: "DevInno @ Enedis",
    dates: "2016/08 - 2017/09",
    company: "Enedis",
    project: "DevInno, DevOps platform team",
    role: "CI/CD platform deployment & management, user support",
    employer: "Groupe Open"
}, {
    title: "Senior Developer",
    caption: "LinkyDep @ Enedis",
    dates: "2013/08 - 2016/07",
    company: "Enedis",
    project: "LinkyDep, nationwide deployment of the French Smart Meter",
    role: "UI, B2B, batch development & optimization in Java",
    employer: "Groupe Open"
}, {
    title: "Lead Developer",
    caption: "BIC/IBAN Converter @ Tessi",
    dates: "2013/05 - 2013/07",
    company: "Tessi",
    project: "BIC/IBAN Converter",
    role: "Design, Java development & acceptance tests",
    employer: "Groupe Open"
}]
