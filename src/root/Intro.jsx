import photo from "./photo.png"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faGithub, faGitlab, faLinkedin, faMastodon, faPython, faUbuntu} from "@fortawesome/free-brands-svg-icons"

const icons = [
    {href: "https://gitlab.com/__mazerty__", icon: faGitlab},
    {href: "https://github.com/mazerty", icon: faGithub},
    {href: "https://launchpad.net/~mazerty", icon: faUbuntu},
    {href: "https://pypi.org/user/mazerty", icon: faPython},
    {href: "https://mastodon.social/@mazerty", icon: faMastodon},
    {href: "https://www.linkedin.com/in/mazerty", icon: faLinkedin}
]

export default () => (
    <section className="min-h-80 flex print:min-h-0">
        <div className="m-auto text-center">
            <img className="h-60 w-60 rounded-full shadow-picture mx-auto mb-5" src={photo} alt="photo"/>
            <div className="font-specialelite text-3xl">Thomas JOUANNOT</div>
            <div className="text-gray-400">Java/Python/JavaScript developer</div>
            <div className="text-gray-400">Software/Cloud architect</div>
            <div className="text-gray-400"><a href="https://lyonjug.org">LyonJUG</a> organizer</div>
            {icons.map(item => (
                <a key={item.href} href={item.href}><FontAwesomeIcon icon={item.icon} className="m-2"/></a>
            ))}
        </div>
    </section>
)
