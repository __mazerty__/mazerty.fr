import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faHeart} from "@fortawesome/free-regular-svg-icons"
import skills from "./skills.js"
import {ChevronDoubleDownIcon, ChevronRightIcon} from "@heroicons/react/24/outline/index.js"
import {Set} from "immutable"
import {useState} from "react"
import {Transition} from "@headlessui/react"

const openableItems = new Set(skills.map(activity => activity.name))

export default () => {
    const [openItems, setOpenItems] = useState(openableItems)

    const areAllItemsOpen = openItems.equals(openableItems)

    return <>
        <div className="pb-3 pt-10 flex items-center">
            <div className="mr-auto font-specialelite text-2xl">Skills <span className="text-gray-400">+ todo</span></div>
            <div className="pl-3 py-1 text-gray-400 cursor-pointer print:hidden">
                <ChevronDoubleDownIcon
                    className={"h-6 w-6 transition-transform transform " + (areAllItemsOpen ? "rotate-180" : "rotate-0")}
                    onClick={() => setOpenItems(areAllItemsOpen ? new Set() : openableItems)}/>
            </div>
        </div>
        <div className="columns-4xs">
            {skills.map(category => {
                const isOpen = openItems.has(category.name)

                return <div key={category.name} className="mb-4 border border-gray-600 rounded break-inside-avoid shadow-diffuse">
                    <div className="px-4 py-3 flex items-center bg-gray-697 cursor-pointer print:bg-transparent" onClick={() => setOpenItems(isOpen ? openItems.delete(category.name) : openItems.add(category.name))}>
                        <ChevronRightIcon className={"h-4 w-4 mr-2 text-gray-400 transition-transform transform " + (isOpen ? "rotate-90" : "rotate-0")}/>
                        <div>{category.name}</div>
                    </div>
                    <Transition
                        show={isOpen}
                        className="overflow-hidden"
                        enter="transition-[max-height] ease-in"
                        enterFrom="max-h-0"
                        enterTo="max-h-96"
                        leave="transition-[max-height] ease-out"
                        leaveFrom="max-h-96"
                        leaveTo="max-h-0">
                        <div className="px-4 py-3 bg-gray-703 print:bg-transparent">
                            {category.skills.map(skill => (
                                <div key={skill.name}>
                                    <div>{skill.name}{skill.like && <FontAwesomeIcon icon={faHeart} className="ml-1 text-red-500"/>}</div>
                                    {skill.score && <div className="h-0.5 -mt-0.5 mb-0.5 bg-gradient-to-r from-gray-600 to-gray-500 print:bg-transparent print:border" style={{width: skill.score + "%"}}/>}
                                </div>
                            ))}
                            {category.todos && <div className="mt-2 text-gray-400">
                                {category.todos.map(todo => (
                                    <div key={todo.name}>{todo.name}{todo.like && <FontAwesomeIcon icon={faHeart} className="ml-1"/>}</div>
                                ))}
                            </div>}
                        </div>
                    </Transition>
                </div>
            })}
        </div>
    </>
}
