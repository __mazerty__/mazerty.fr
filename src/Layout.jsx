import {Suspense, useEffect} from "react"
import {Link, Outlet, ScrollRestoration, useMatches} from "react-router"
import {Popover} from "@headlessui/react"
import {Set} from "immutable"
import Loading from "./Loading"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faChevronDown} from "@fortawesome/free-solid-svg-icons"
import {faCreativeCommons, faCreativeCommonsBy, faCreativeCommonsNcEu, faCreativeCommonsSa} from "@fortawesome/free-brands-svg-icons"
import Logo from "./logo.svg?react"

export default () => {
    const current_path = useMatches()[1].pathname
    const subtitle = useMatches()[3]?.handle?.title
    const paths = Set.of(current_path, "/", "/blog", "/lynx")

    useEffect(() => {
        document.title = (subtitle ? subtitle + " - " : "") + "mazerty.fr" + (current_path !== "/" ? current_path : "")
    }, [subtitle, current_path])

    return <>
        <ScrollRestoration/>
        <nav className="bg-gray-50 text-gray-900 fixed w-full px-5 z-10 print:hidden">
            <div className="max-w-4xl mx-auto flex items-center h-16 text-xl">
                <Link to="/" className="flex">
                    <Logo className="h-7 w-7 mr-1.5"/>
                    <div>mazerty.fr</div>
                </Link>
                <Popover className="relative">
                    {current_path !== "/" && <Link to={current_path} className="ml-2 text-gray-500">{current_path}</Link>}
                    <Popover.Button className="ml-1 px-2 text-base text-gray-400 outline-none"><FontAwesomeIcon icon={faChevronDown}/></Popover.Button>
                    <Popover.Panel className="absolute -top-2 flex flex-col bg-gray-50 p-1 outline outline-1 outline-gray-200 rounded">
                        {paths.map(path =>
                            <Popover.Button as={Link} to={path} key={path} className="p-1 pr-8 hover:text-gray-500 text-nowrap">{path}</Popover.Button>
                        )}
                    </Popover.Panel>
                </Popover>
            </div>
        </nav>
        <main className="flex flex-col bg-gray-700 text-gray-200 min-h-screen px-5 pt-20 print:text-gray-900 print:bg-transparent">
            <div className="w-full max-w-4xl mx-auto">
                <Suspense fallback={<Loading/>}><Outlet/></Suspense>
            </div>
            <footer className="mt-auto pt-20 text-gray-400 text-xs text-center print:hidden">
                <div><FontAwesomeIcon icon={faCreativeCommons}/> <FontAwesomeIcon icon={faCreativeCommonsBy}/> <FontAwesomeIcon icon={faCreativeCommonsNcEu}/> <FontAwesomeIcon icon={faCreativeCommonsSa}/></div>
                <div>Licensed under the <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>
                <div>Powered by <a href="https://react.dev">React</a>, <a href="https://vitejs.dev">Vite</a>, <a href="https://tailwindcss.com">Tailwind</a>, <a href="https://fontsource.org">Fontsource</a>, <a href="https://fontawesome.com">Font Awesome</a> and <a href="https://gitlab.com/__mazerty__/mazerty.fr">GitLab</a>.</div>
            </footer>
        </main>
    </>
}
