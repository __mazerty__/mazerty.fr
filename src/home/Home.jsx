import {Transition} from "@headlessui/react"
import {ChevronRightIcon} from "@heroicons/react/24/outline"
import bookmarks from "./bookmarks.js"
import {Set} from "immutable"
import {useState} from "react"

const initialOpenItems = new Set(bookmarks.filter(category => category.open).map(category => category.title))

export default () => {
    const [openItems, setOpenItems] = useState(initialOpenItems)

    return <div className="border border-gray-600 rounded divide-y divide-gray-600 shadow-diffuse">
        {bookmarks.map(category => {
            const isOpen = openItems.has(category.title)

            return <div key={category.title}>
                <div className="px-4 py-3 flex items-center bg-gray-697 cursor-pointer" onClick={() => setOpenItems(isOpen ? openItems.delete(category.title) : openItems.add(category.title))}>
                    <ChevronRightIcon className={"h-4 w-4 mr-2 text-gray-400 transition-transform transform " + (isOpen ? "rotate-90" : "rotate-0")}/>
                    <div>{category.title}</div>
                </div>
                <Transition
                    show={isOpen}
                    className="overflow-hidden"
                    enter="transition-[max-height] ease-in"
                    enterFrom="max-h-0"
                    enterTo="max-h-96"
                    leave="transition-[max-height] ease-out"
                    leaveFrom="max-h-96"
                    leaveTo="max-h-0">
                    <div className="px-5 py-4 bg-gray-703 text-gray-400">
                        {category.bookmarks.map(bookmark => (
                            <div key={bookmark.title} className="truncate">
                                <a href={bookmark.url} className="mr-3 text-gray-200 hover:underline">{bookmark.title}</a>
                                <a href={bookmark.url} className="hover:underline">{bookmark.url}</a>
                            </div>
                        ))}
                    </div>
                </Transition>
            </div>
        })}
    </div>
}
