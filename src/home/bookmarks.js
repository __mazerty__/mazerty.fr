export default [{
    title: "Main",
    open: true,
    bookmarks: [
        {title: "Gmail", url: "https://mail.google.com"},
        {title: "Feedly", url: "https://feedly.com"},
        {title: "Mastodon", url: "https://mastodon.social"},
        {title: "GitHub", url: "https://github.com"},
        {title: "GitLab", url: "https://gitlab.com"},
        {title: "Météo", url: "https://meteofrance.com"},
        {title: "Fortuneo", url: "https://mabanque.fortuneo.fr"},
        {title: "LinkedIn", url: "https://linkedin.com"}
    ]
}, {
    title: "Movies",
    bookmarks: [
        {title: "Comoedia", url: "https://www.cinema-comoedia.com/a-l-affiche-au-comoedia"},
        {title: "UGC Part-Dieu", url: "https://www.ugc.fr/cinema-ugc-cine-cite-part-dieu.html"},
        {title: "UGC Confluence", url: "https://www.ugc.fr/cinema-ugc-cine-cite-confluence.html"},
        {title: "UGC Cité Internationale", url: "https://www.ugc.fr/cinema-ugc-cine-cite-internationale-lyon.html"},
        {title: "UGC Astoria", url: "https://www.ugc.fr/cinema-ugc-astoria.html"}
    ]
}, {
    title: "Handy",
    bookmarks: [
        {title: "AWS", url: "https://mazerty.signin.aws.amazon.com/console"},
        {title: "Speed test", url: "https://speed.cloudflare.com"},
        {title: "Tunefind", url: "https://www.tunefind.com"},
        {title: "Morrowind Map", url: "https://en.uesp.net/maps/mwmap/mwmap.shtml"},
        {title: "SubsPlease", url: "https://subsplease.org"},
        {title: "YTS YIFY Status", url: "https://yifystatus.com"},
        {title: "EZTV Status", url: "https://eztvstatus.org"},
        {title: "RARBG Status", url: "https://rarbg.tw"},
        {title: "1337x Status", url: "https://1337x-status.org"},
        {title: "TGx Status", url: "https://proxygalaxy.me"}
    ]
}]
